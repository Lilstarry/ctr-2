import $ from "jquery";
import * as rxjs from "rxjs";
import { setTimer } from "./timer";

const notes = 4;

export const modalHandler = (event, id, points, sliderObject) => {
    let page = document.getElementsByClassName('ctr-page--device')[0];
    let temp = document.getElementById(id);

    let div = document.createElement("div");
    div.className='fade-black';
    page.appendChild(div);

    page.appendChild(temp.content);
    points.add(id);
    let crossButton = document.getElementsByClassName('btn-cross')[0];
    rxjs.fromEvent(crossButton, "click")
    .subscribe(event =>closeModal(event, id, points, sliderObject));
}

const closeModal = (event, id, points, sliderObject) => {
    let modalChild = document.getElementsByClassName('modal')[0];
    let temp = document.getElementById(id);
    temp = temp.content;
    temp.appendChild(modalChild);

    let node = document.getElementsByClassName('fade-black')[0];
    node.parentNode.removeChild(node);

    if(points.size == notes) {
        if (sliderObject) {
            sliderObject.next();
            setTimer(sliderObject);
        } else {
            throw Error('Slider not defined');
        }
    }
}