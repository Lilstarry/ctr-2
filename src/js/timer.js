import $ from "jquery";
const SEC = 1000;
const timeOut = SEC * 10;
let result = 0;

const timerId = setInterval(() => {
  result++;
}, SEC);

export const setTimer = (sliderObject) => {
  result = 0;
  setTimeout(() => {
    clearInterval(timerId);
    sliderObject.select(0);
    $("#play-wrap")[0].style.opacity = 1;
  }, timeOut);
};
