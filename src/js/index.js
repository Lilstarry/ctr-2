import FlickityFull from "flickity-fullscreen";
import * as rxjs from "rxjs";
import $ from "jquery";
import { startButtonHandler, skipButtonHandler, playButtonHandler} from "./eventHandlers";
import { modalHandler } from "./modalHandlers";

export const flkt = new FlickityFull(".main-carousel", {
  cellAlign: "center",
  fullscreen: true,
  draggable: false,
  prevNextButtons: false,
  pageDots: false,
  freeScroll: false
});

const playButton = document.getElementById("play-button");
rxjs.fromEvent(playButton, "click")
.subscribe(event => playButtonHandler(event));

const skipVideoButton = document.getElementById("skip-video-button");
rxjs.fromEvent(skipVideoButton, "click")
.subscribe(event => skipButtonHandler(event, flkt));

const initialButton = document.getElementById("initial-button");
rxjs.fromEvent(initialButton, "click")
.subscribe(event => startButtonHandler(event, flkt));

let points = new Set();

const roundbtnIroning = document.getElementById("ironing");
rxjs.fromEvent(roundbtnIroning, "click")
.subscribe(event => modalHandler(event, "ironing-modal", points, flkt));

const roundbtnClamp = document.getElementById("clamp");
rxjs.fromEvent(roundbtnClamp, "click")
.subscribe(event => modalHandler(event, "clamp-modal", points, flkt));

const roundbtnMultilevel = document.getElementById("multilevel");
rxjs.fromEvent(roundbtnMultilevel, "click")
.subscribe(event => modalHandler(event, "multilevel-modal", points, flkt));

const roundbtnPolymetacid = document.getElementById("polymetacid");
rxjs.fromEvent(roundbtnPolymetacid, "click")
.subscribe(event => modalHandler(event, "polymetacid-modal", points, flkt));
